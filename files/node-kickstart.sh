#!/bin/bash
set -Eeuo pipefail
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}

[ -e /boot/mygw ] && panic "mygw file already exists, quitting" || /bin/true

_srv=$(get_my_tftp_server) || debug "failed to get tftp server" || exit 1
_nic=$(get_my_uplink_ip6_nic) || debug "failed to get uplink" || exit 1
_mac=$(get_mac_from_ifname ${_nic}) || debug "failed to get mac for nic ${_nic}" || exit 1
_my_init=$(mktemp -t my-init.XXX) || debug "failed to create temp file" || exit 1

atftp --get --remote-file /boot/nodes/${_mac}.mygw --local-file /boot/mygw ${_srv} 69
apt-get update
apt-get -y install my-vminstaller
#sgdisk --zap-all /dev/sda
#sgdisk --zap-all /dev/sdb
MY_DEV=/dev/sda MY_CONFIG=/boot/mygw MY_ROOT_SIZE="+20G" mkinstall yes
reboot
